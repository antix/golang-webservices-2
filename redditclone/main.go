package main

import (
	"log"
	"net/http"
	"redditclone/pkg/entity"
	"redditclone/pkg/handlers"
	"redditclone/pkg/middleware"
	"redditclone/pkg/repository/comment"
	"redditclone/pkg/repository/post"
	"redditclone/pkg/repository/user"
	"redditclone/pkg/repository/vote"

	"github.com/gorilla/mux"
)

func main() {
	router := mux.NewRouter()
	router.Use(middleware.AccessLog)

	// User routes
	userStorage, err := user.NewUserStorage(&user.Config{StorageType: user.StorageTypeMemory})
	if err != nil {
		log.Fatalf("Error initializing user storage: %v\n", err)
	}
	userHandler := handlers.UserHandler{UserRepo: userStorage}
	userRoutes := router.Headers("Content-Type", "application/json")
	userSubRouter := userRoutes.Subrouter()
	userSubRouter.HandleFunc("/api/register", userHandler.Register).Methods("POST") // №1
	userSubRouter.HandleFunc("/api/login", userHandler.Login).Methods("POST")       // №2

	// Posts routes
	postStorage, err := post.NewPostStorage(&post.Config{StorageType: post.StorageTypeMemory})
	if err != nil {
		log.Fatalf("Error initializing post storage: %v\n", err)
	}
	voteStorage, err := vote.NewVoteStorage(&vote.Config{StorageType: vote.StorageTypeMemory})
	if err != nil {
		log.Fatalf("Error initializing vote storage: %v\n", err)
	}
	commentStorage, err := comment.NewCommentStorage(&comment.Config{StorageType: comment.StorageTypeMemory})
	if err != nil {
		log.Fatalf("Error initializing comment storage: %v\n", err)
	}
	postHandler := handlers.PostHandler{PostRepo: postStorage, UserRepo: userStorage, VoteRepo: voteStorage, CommentRepo: commentStorage}
	// unauthorized routes
	router.HandleFunc("/api/posts/", postHandler.List).Methods("GET")               // №3
	router.HandleFunc("/api/posts/{category}", postHandler.Category).Methods("GET") // №5
	router.HandleFunc("/api/post/{post_id}", postHandler.Get).Methods("GET")        // №6
	router.HandleFunc("/api/user/{username}", postHandler.User).Methods("GET")      // №13
	// authorized routes
	// json routes
	postAuthJsonRoutes := router.Headers("Content-Type", "application/json")
	postAuthJsonSubRouter := postAuthJsonRoutes.Subrouter()
	postAuthJsonSubRouter.Use(middleware.Auth)
	postAuthJsonSubRouter.HandleFunc("/api/posts", postHandler.Create).Methods("POST")                                // №4
	postAuthJsonSubRouter.HandleFunc("/api/post/{post_id}", postHandler.Remove).Methods("DELETE")                     // №12
	postAuthJsonSubRouter.HandleFunc("/api/post/{post_id}", postHandler.AddComment).Methods("POST")                   // №7
	postAuthJsonSubRouter.HandleFunc("/api/post/{post_id}/{comment_id}", postHandler.RemoveComment).Methods("DELETE") // №8
	// plain text routes
	postAuthSubRouter := router.NewRoute().Subrouter()
	postAuthSubRouter.Use(middleware.Auth)
	postAuthSubRouter.HandleFunc("/api/post/{post_id}/upvote", postHandler.Vote(entity.Upvote)).Methods("GET")     // №9
	postAuthSubRouter.HandleFunc("/api/post/{post_id}/downvote", postHandler.Vote(entity.Downvote)).Methods("GET") // №10
	postAuthSubRouter.HandleFunc("/api/post/{post_id}/unvote", postHandler.Vote(entity.Unvote)).Methods("GET")     // №11

	// Static files routes
	// single page application routes
	indexHandler := func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, "./web/static/index.html")
	}
	router.HandleFunc("/", indexHandler).Methods("GET")
	router.NotFoundHandler = http.HandlerFunc(indexHandler)
	// static files
	staticHandler := http.FileServer(http.Dir("./web/static"))
	router.PathPrefix("/static").Handler(http.StripPrefix("/", staticHandler))
	router.PathPrefix("/favicon.ico").Handler(http.StripPrefix("/", staticHandler))

	log.Println("starting server at :8080")
	if err := http.ListenAndServe(":8080", router); err != nil {
		log.Fatalf("Error starting server: %v\n", err)
	}
}
