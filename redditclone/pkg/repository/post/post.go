package post

import (
	"context"
	"errors"
	"io"
	"redditclone/pkg/entity"
	"redditclone/pkg/repository/post/storage/memory"
)

const (
	StorageTypeMemory string = "mem"
)

type Config struct {
	StorageType string
}

type PostStorage interface {
	Create(ctx context.Context, u *entity.Post) error
	Update(ctx context.Context, u entity.Post) error
	UpdateViews(ctx context.Context, id string, views int) error
	Remove(ctx context.Context, id string) error
	GetAll(ctx context.Context) ([]entity.Post, error)
	GetByID(ctx context.Context, id string) (entity.Post, error)
	GetByAuthorID(ctx context.Context, authorID string) ([]entity.Post, error)
	GetByCategory(ctx context.Context, category string) ([]entity.Post, error)
	io.Closer
}

func NewPostStorage(cfg *Config) (PostStorage, error) {
	switch cfg.StorageType {
	case StorageTypeMemory:
		return memory.NewMemoryStorage()
	default:
		return nil, errors.New("unknown storage type")
	}
}
