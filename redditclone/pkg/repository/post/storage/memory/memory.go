package memory

import (
	"cmp"
	"context"
	"encoding/json"
	"redditclone/pkg/entity"
	"slices"
	"sync"

	"github.com/google/uuid"
)

type memoryStorage struct {
	data  map[string]entity.Post
	mutex sync.RWMutex
}

// NewMemoryStorage создает новое хранилище данных в памяти.
func NewMemoryStorage() (*memoryStorage, error) {
	data := make(map[string]entity.Post)
	_ = json.Unmarshal([]byte(exampleData), &data)
	return &memoryStorage{
		//data: make(map[string]entity.Post),
		data: data,
	}, nil
}

func (s *memoryStorage) Create(_ context.Context, p *entity.Post) error {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	p.ID = uuid.NewString()
	s.data[p.ID] = *p
	return nil
}

func (s *memoryStorage) Update(_ context.Context, p entity.Post) error {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	s.data[p.ID] = p
	return nil
}

func (s *memoryStorage) UpdateViews(ctx context.Context, id string, views int) error {
	p, err := s.GetByID(ctx, id)
	if err != nil {
		return err
	}
	p.Views = views
	return s.Update(ctx, p)
}

func (s *memoryStorage) Remove(_ context.Context, id string) error {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	delete(s.data, id)
	return nil
}

func (s *memoryStorage) GetByID(_ context.Context, id string) (entity.Post, error) {
	s.mutex.RLock()
	defer s.mutex.RUnlock()

	p, ok := s.data[id]
	if !ok {
		return entity.Post{}, entity.ErrPostNotFound
	}
	return p, nil
}

func (s *memoryStorage) GetByAuthorID(_ context.Context, authorID string) ([]entity.Post, error) {
	s.mutex.RLock()
	defer s.mutex.RUnlock()

	posts := make([]entity.Post, 0)
	for _, p := range s.data {
		if p.AuthorID == authorID {
			posts = append(posts, p)
		}
	}
	slices.SortFunc(posts, func(a, b entity.Post) int {
		return cmp.Compare(b.Created.Unix(), a.Created.Unix())
	})
	return posts, nil
}

func (s *memoryStorage) GetByCategory(_ context.Context, category string) ([]entity.Post, error) {
	s.mutex.RLock()
	defer s.mutex.RUnlock()

	posts := make([]entity.Post, 0)
	for _, p := range s.data {
		if p.Category == category {
			posts = append(posts, p)
		}
	}
	slices.SortFunc(posts, func(a, b entity.Post) int {
		return cmp.Compare(b.Score, a.Score)
	})
	return posts, nil
}

func (s *memoryStorage) GetAll(_ context.Context) ([]entity.Post, error) {
	s.mutex.RLock()
	defer s.mutex.RUnlock()

	posts := make([]entity.Post, 0, len(s.data))
	for _, p := range s.data {
		posts = append(posts, p)
	}
	slices.SortFunc(posts, func(a, b entity.Post) int {
		return cmp.Compare(b.Score, a.Score)
	})
	return posts, nil
}

func (s *memoryStorage) Close() error {
	return nil
}

const exampleData = `
{
    "67a8a5aca9deec0025911fd7": {
        "authorID": "d33aa94f-a60d-4291-aa72-3905e9c1c817",
        "score": 2,
        "views": 6,
        "type": "text",
        "title": "aaa",
        "category": "music",
        "text": "bbbbbbbbb",
        "comments": [],
        "created": "2025-02-09T12:55:08.186Z",
        "upvotePercentage": 100,
        "id": "67a8a5aca9deec0025911fd7"
    },
    "67ae357089733a00253e4822":{
        "authorID": "d33aa94f-a60d-4291-aa72-3905e9c1c817",
        "score": 2,
        "views": 1,
        "type": "text",
        "title": "hello",
        "category": "music",
        "text": "good day",
        "comments": [
            {
                "created": "2025-02-13T18:12:01.410Z",
                "author": {
                    "username": "admin",
                    "id": "d33aa94f-a60d-4291-aa72-3905e9c1c817"
                },
                "body": "all good",
                "id": "67ae35f189733a00253e4823"
            }
        ],
        "created": "2025-02-13T18:09:52.563Z",
        "upvotePercentage": 100,
        "id": "67ae357089733a00253e4822"
    },
    "67b4cf99f2258b001ee7ca93":{
        "authorID": "84b0ab7f-0c7e-4f58-967d-ca6633719764",
        "score": 1,
        "views": 1,
        "type": "text",
        "title": "kkkkkkkkkkk",
        "category": "videos",
        "text": "lllllllllllll",
        "comments": [
            {
                "created": "2025-02-18T18:22:18.994Z",
                "author": {
                    "username": "test",
                    "id": "84b0ab7f-0c7e-4f58-967d-ca6633719764"
                },
                "body": "my comment",
                "id": "67b4cfdaf2258b001ee7cabb"
            }
        ],
        "created": "2025-02-18T18:21:13.753Z",
        "upvotePercentage": 100,
        "id": "67b4cf99f2258b001ee7ca93"
    },
    "67a8a697a9deec0025911fd8":{
        "authorID": "d33aa94f-a60d-4291-aa72-3905e9c1c817",
        "score": -2,
        "views": 5,
        "type": "link",
        "title": "ggggg",
        "url": "http://gggggg.by",
        "category": "funny",
        "comments": [
            {
                "created": "2025-02-09T13:00:00.970Z",
                "author": {
                    "username": "admin",
                    "id": "d33aa94f-a60d-4291-aa72-3905e9c1c817"
                },
                "body": "test",
                "id": "67a8a6d0a9deec0025911fd9"
            },
            {
                "created": "2025-02-09T13:00:14.859Z",
                "author": {
                    "username": "admin",
                    "id": "d33aa94f-a60d-4291-aa72-3905e9c1c817"
                },
                "body": "hi hi hi",
                "id": "67a8a6dea9deec0025911fda"
            },
            {
                "created": "2025-02-18T18:22:18.994Z",
                "author": {
                    "username": "test",
                    "id": "84b0ab7f-0c7e-4f58-967d-ca6633719764"
                },
                "body": "type PostHandler struct {\n\tPostRepo storage.PostStorage\n}",
                "id": "67b4cfdaf2258b001ee7ca97"
            }
        ],
        "created": "2025-02-09T12:59:03.478Z",
        "upvotePercentage": 0,
        "id": "67a8a697a9deec0025911fd8"
    }
}
`
