package user

import (
	"context"
	"errors"
	"io"
	"redditclone/pkg/entity"
	"redditclone/pkg/repository/user/storage/memory"
)

const (
	StorageTypeMemory string = "mem"
)

type Config struct {
	StorageType string
}

type UserStorage interface {
	Create(ctx context.Context, u *entity.User) error
	GetByID(ctx context.Context, id string) (entity.User, error)
	GetByName(ctx context.Context, name string) (entity.User, error)
	GetAll(ctx context.Context) ([]entity.User, error)
	io.Closer
}

func NewUserStorage(cfg *Config) (UserStorage, error) {
	switch cfg.StorageType {
	case StorageTypeMemory:
		return memory.NewMemoryStorage()
	default:
		return nil, errors.New("unknown storage type")
	}
}

func GetAuthor(ctx context.Context, userRepo UserStorage, authorID string) (entity.Author, error) {
	user, err := userRepo.GetByID(ctx, authorID)
	if err != nil {
		return entity.Author{}, err
	}
	return entity.Author{Username: user.Username, AuthorID: user.ID}, nil
}
