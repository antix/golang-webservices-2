package memory

import (
	"context"
	"errors"
	"redditclone/pkg/entity"
	"sync"

	"github.com/google/uuid"
)

type memoryStorage struct {
	data  map[string]entity.User
	mutex sync.RWMutex
}

var (
	ErrUserNotFound = errors.New("user not found")
)

// NewMemoryStorage создает новое хранилище данных в памяти.
func NewMemoryStorage() (*memoryStorage, error) {
	users := map[string]entity.User{
		"d33aa94f-a60d-4291-aa72-3905e9c1c817": {
			ID:       "d33aa94f-a60d-4291-aa72-3905e9c1c817",
			Username: "admin",
			Password: "1q2w3e4r",
		},
		"84b0ab7f-0c7e-4f58-967d-ca6633719764": {
			ID:       "84b0ab7f-0c7e-4f58-967d-ca6633719764",
			Username: "test",
			Password: "1q2w3e4r",
		},
	}
	return &memoryStorage{
		data: users,
		//data: make(map[string]entity.User),
		mutex: sync.RWMutex{},
	}, nil
}

func (s *memoryStorage) Create(_ context.Context, u *entity.User) error {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	u.ID = uuid.NewString()
	s.data[u.ID] = *u
	return nil
}

func (s *memoryStorage) GetByID(_ context.Context, id string) (entity.User, error) {
	s.mutex.RLock()
	defer s.mutex.RUnlock()

	p, ok := s.data[id]
	if !ok {
		return entity.User{}, ErrUserNotFound
	}
	return p, nil
}

func (s *memoryStorage) GetByName(_ context.Context, name string) (entity.User, error) {
	s.mutex.RLock()
	defer s.mutex.RUnlock()

	for _, u := range s.data {
		if u.Username == name {
			return u, nil
		}
	}
	return entity.User{}, ErrUserNotFound
}

func (s *memoryStorage) GetAll(_ context.Context) ([]entity.User, error) {
	s.mutex.RLock()
	defer s.mutex.RUnlock()

	posts := make([]entity.User, 0, len(s.data))
	for _, p := range s.data {
		posts = append(posts, p)
	}
	return posts, nil
}

func (s *memoryStorage) Close() error {
	return nil
}
