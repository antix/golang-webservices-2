package memory

import (
	"context"
	"encoding/json"
	"redditclone/pkg/entity"
	"sync"
)

type memoryStorage struct {
	data  map[string]*voteContainer
	mutex sync.RWMutex
}

type voteContainer struct {
	sliceMutex sync.RWMutex
	Votes      []entity.Vote
}

// NewMemoryStorage создает новое хранилище данных в памяти.
func NewMemoryStorage() (*memoryStorage, error) {
	data := make(map[string]*voteContainer)
	_ = json.Unmarshal([]byte(exampleData), &data)
	return &memoryStorage{
		//data: make(map[string]*voteContainer),
		data: data,
	}, nil
}

func (s *memoryStorage) Close() error {
	return nil
}

func (s *memoryStorage) Add(ctx context.Context, postID string, v entity.Vote) error {
	s.mutex.Lock()
	container, exists := s.data[postID]
	if !exists {
		container = &voteContainer{
			Votes: make([]entity.Vote, 0),
		}
		s.data[postID] = container
	}
	s.mutex.Unlock()

	container.sliceMutex.Lock()
	container.Votes = append(container.Votes, v)
	container.sliceMutex.Unlock()

	return nil
}

func (s *memoryStorage) GetAll(ctx context.Context, postID string) ([]entity.Vote, error) {
	container, err := s.getContainer(ctx, postID)
	if err != nil {
		return nil, err
	}

	container.sliceMutex.RLock()
	defer container.sliceMutex.RUnlock()

	// return copy to avoid outside modification
	votes := make([]entity.Vote, len(container.Votes))
	copy(votes, container.Votes)
	return votes, nil
}

func (s *memoryStorage) getContainer(_ context.Context, postID string) (*voteContainer, error) {
	s.mutex.RLock()
	defer s.mutex.RUnlock()
	container, ok := s.data[postID]
	if !ok {
		return nil, entity.ErrVoteNotFound
	}

	return container, nil
}

func (s *memoryStorage) GetByAuthorID(ctx context.Context, postID string, authorID string) (entity.Vote, error) {
	container, err := s.getContainer(ctx, postID)
	if err != nil {
		return entity.Vote{}, err
	}

	container.sliceMutex.RLock()
	defer container.sliceMutex.RUnlock()

	for _, vote := range container.Votes {
		if vote.AuthorID == authorID {
			return vote, nil
		}
	}
	return entity.Vote{}, entity.ErrVoteNotFound
}

func (s *memoryStorage) RemoveByAuthorID(ctx context.Context, postID string, authorID string) error {
	container, err := s.getContainer(ctx, postID)
	if err != nil {
		return err
	}

	container.sliceMutex.Lock()
	defer container.sliceMutex.Unlock()

	for i, vote := range container.Votes {
		if vote.AuthorID == authorID {
			container.Votes = append(container.Votes[:i], container.Votes[i+1:]...)
			return nil
		}
	}

	return entity.ErrVoteNotFound
}

func (s *memoryStorage) UpdateByAuthorID(ctx context.Context, postID string, authorID string, newVote entity.VoteValue) error {
	container, err := s.getContainer(ctx, postID)
	if err != nil {
		return err
	}

	container.sliceMutex.Lock()
	defer container.sliceMutex.Unlock()

	for i, vote := range container.Votes {
		if vote.AuthorID == authorID {
			container.Votes[i].Vote = newVote
			return nil
		}
	}
	return entity.ErrVoteNotFound
}

func (s *memoryStorage) RemoveAll(ctx context.Context, postID string) error {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	delete(s.data, postID)
	return nil
}

const exampleData = `
{
    "67a8a5aca9deec0025911fd7": {
        "votes": [
            {
                "user": "d33aa94f-a60d-4291-aa72-3905e9c1c817",
                "vote": 1
            },
            {
                "user": "84b0ab7f-0c7e-4f58-967d-ca6633719764",
                "vote": 1
            }
        ]
    },
    "67ae357089733a00253e4822":{
        "votes": [
            {
                "user": "d33aa94f-a60d-4291-aa72-3905e9c1c817",
                "vote": 1
            },
            {
                "user": "84b0ab7f-0c7e-4f58-967d-ca6633719764",
                "vote": 1
            }
        ]
    },
    "67b4cf99f2258b001ee7ca93":{
        "votes": [
            {
                "user": "84b0ab7f-0c7e-4f58-967d-ca6633719764",
                "vote": 1
            }
        ]
    },
    "67a8a697a9deec0025911fd8":{
        "votes": [
            {
                "user": "d33aa94f-a60d-4291-aa72-3905e9c1c817",
                "vote": -1
            },
            {
                "user": "84b0ab7f-0c7e-4f58-967d-ca6633719764",
                "vote": -1
            }
        ]
    }
}
`
