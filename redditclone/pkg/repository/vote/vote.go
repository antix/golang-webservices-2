package vote

import (
	"context"
	"errors"
	"io"
	"redditclone/pkg/entity"
	"redditclone/pkg/repository/vote/storage/memory"
)

const (
	StorageTypeMemory string = "mem"
)

type Config struct {
	StorageType string
}

type VoteStorage interface {
	Add(ctx context.Context, postID string, v entity.Vote) error
	GetAll(ctx context.Context, postID string) ([]entity.Vote, error)
	RemoveAll(ctx context.Context, postID string) error
	GetByAuthorID(ctx context.Context, postID string, authorID string) (entity.Vote, error)
	RemoveByAuthorID(ctx context.Context, postID string, authorID string) error
	UpdateByAuthorID(ctx context.Context, postID string, authorID string, newVote entity.VoteValue) error
	io.Closer
}

func NewVoteStorage(cfg *Config) (VoteStorage, error) {
	switch cfg.StorageType {
	case StorageTypeMemory:
		return memory.NewMemoryStorage()
	default:
		return nil, errors.New("unknown storage type")
	}
}
