package memory

import (
	"context"
	"encoding/json"
	"redditclone/pkg/entity"
	"sync"

	"github.com/google/uuid"
)

type memoryStorage struct {
	data  map[string]*commentContainer
	mutex sync.RWMutex
}

type commentContainer struct {
	sliceMutex sync.RWMutex
	Comments   []entity.Comment
}

// NewMemoryStorage создает новое хранилище данных в памяти.
func NewMemoryStorage() (*memoryStorage, error) {
	data := make(map[string]*commentContainer)
	_ = json.Unmarshal([]byte(exampleData), &data)
	return &memoryStorage{
		//data: make(map[string]*commentContainer),
		data: data,
	}, nil
}

func (s *memoryStorage) Close() error {
	return nil
}

func (s *memoryStorage) Add(ctx context.Context, postID string, c *entity.Comment) error {
	s.mutex.Lock()
	container, exists := s.data[postID]
	if !exists {
		container = &commentContainer{
			Comments: make([]entity.Comment, 0),
		}
		s.data[postID] = container
	}
	s.mutex.Unlock()

	c.ID = uuid.NewString()
	container.sliceMutex.Lock()
	container.Comments = append(container.Comments, *c)
	container.sliceMutex.Unlock()

	return nil
}

func (s *memoryStorage) GetAll(ctx context.Context, postID string) ([]entity.Comment, error) {
	container, err := s.getContainer(ctx, postID)
	if err != nil {
		return nil, err
	}

	container.sliceMutex.RLock()
	defer container.sliceMutex.RUnlock()

	// return copy to avoid outside modification
	comments := make([]entity.Comment, len(container.Comments))
	copy(comments, container.Comments)
	return comments, nil
}

func (s *memoryStorage) getContainer(_ context.Context, postID string) (*commentContainer, error) {
	s.mutex.RLock()
	defer s.mutex.RUnlock()
	container, ok := s.data[postID]
	if !ok {
		return nil, entity.ErrVoteNotFound
	}

	return container, nil
}

func (s *memoryStorage) GetByID(ctx context.Context, postID string, commentID string) (entity.Comment, error) {
	container, err := s.getContainer(ctx, postID)
	if err != nil {
		return entity.Comment{}, err
	}

	container.sliceMutex.RLock()
	defer container.sliceMutex.RUnlock()

	for _, comment := range container.Comments {
		if comment.ID == commentID {
			return comment, nil
		}
	}
	return entity.Comment{}, entity.ErrVoteNotFound
}

func (s *memoryStorage) Remove(ctx context.Context, postID string, commentID string) error {
	container, err := s.getContainer(ctx, postID)
	if err != nil {
		return err
	}

	container.sliceMutex.Lock()
	defer container.sliceMutex.Unlock()

	for i, comment := range container.Comments {
		if comment.ID == commentID {
			container.Comments = append(container.Comments[:i], container.Comments[i+1:]...)
			return nil
		}
	}

	return entity.ErrVoteNotFound
}

func (s *memoryStorage) RemoveAll(ctx context.Context, postID string) error {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	delete(s.data, postID)
	return nil
}

const exampleData = `
{
    "67a8a5aca9deec0025911fd7": {
        "comments": []
    },
    "67ae357089733a00253e4822":{
        "comments": [
            {
                "created": "2025-02-13T18:12:01.410Z",
                "authorID": "d33aa94f-a60d-4291-aa72-3905e9c1c817",
                "body": "all good",
                "id": "67ae35f189733a00253e4823"
            }
        ]
    },
    "67b4cf99f2258b001ee7ca93":{
        "comments": [
            {
                "created": "2025-02-18T18:22:18.994Z",
                "authorID": "84b0ab7f-0c7e-4f58-967d-ca6633719764",
                "body": "my comment",
                "id": "67b4cfdaf2258b001ee7cabb"
            }
        ]
    },
    "67a8a697a9deec0025911fd8":{
        "comments": [
            {
                "created": "2025-02-09T13:00:00.970Z",
				"authorID": "d33aa94f-a60d-4291-aa72-3905e9c1c817",
                "body": "test",
                "id": "67a8a6d0a9deec0025911fd9"
            },
            {
                "created": "2025-02-09T13:00:14.859Z",
				"authorID": "d33aa94f-a60d-4291-aa72-3905e9c1c817",
                "body": "hi hi hi",
                "id": "67a8a6dea9deec0025911fda"
            },
            {
                "created": "2025-02-18T18:22:18.994Z",
				"authorID": "84b0ab7f-0c7e-4f58-967d-ca6633719764",
                "body": "type PostHandler struct {\n\tPostRepo storage.PostStorage\n}",
                "id": "67b4cfdaf2258b001ee7ca97"
            }
        ]
    }
}
`
