package comment

import (
	"context"
	"errors"
	"io"
	"redditclone/pkg/entity"
	"redditclone/pkg/repository/comment/storage/memory"
)

const (
	StorageTypeMemory string = "mem"
)

type Config struct {
	StorageType string
}

type CommentStorage interface {
	Add(ctx context.Context, postID string, v *entity.Comment) error
	GetByID(ctx context.Context, postID string, commentID string) (entity.Comment, error)
	GetAll(ctx context.Context, postID string) ([]entity.Comment, error)
	Remove(ctx context.Context, postID string, commentID string) error
	RemoveAll(ctx context.Context, postID string) error
	io.Closer
}

func NewCommentStorage(cfg *Config) (CommentStorage, error) {
	switch cfg.StorageType {
	case StorageTypeMemory:
		return memory.NewMemoryStorage()
	default:
		return nil, errors.New("unknown storage type")
	}
}
