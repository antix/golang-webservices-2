package handlers

import (
	"encoding/json"
	"io"
	"log"
	"net/http"
	"redditclone/pkg/config"
	"redditclone/pkg/entity"
	"redditclone/pkg/repository/user"
	"redditclone/pkg/validator"
	"time"

	"github.com/golang-jwt/jwt/v5"
)

type UserHandler struct {
	UserRepo user.UserStorage
}

type JwtUser struct {
	ID       string `json:"id"`
	Username string `json:"username"`
}

type JwtUserClaims struct {
	User JwtUser `json:"user"`
	jwt.RegisteredClaims
}

func (u *UserHandler) Login(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	body, _ := io.ReadAll(r.Body)
	r.Body.Close()

	inUser := new(entity.User)
	err := json.Unmarshal(body, inUser)
	if err != nil {
		WriteResponse(w, err.Error(), http.StatusBadRequest)
		return
	}

	validationErrors := validator.ValidateStruct(inUser)
	if len(validationErrors) > 0 {
		customErrors := make([]*ApiCustomError, 0, len(validationErrors))
		for _, validationError := range validationErrors {
			log.Println(validationError)
			customErrors = append(customErrors, &ApiCustomError{
				Msg:      validationError.Cause,
				Location: "body",
				Param:    validationError.Field,
				Value:    validationError.Value,
			})
		}
		WriteErrorsResponse(w, customErrors, http.StatusBadRequest)
		return
	}

	user, err := u.UserRepo.GetByName(r.Context(), inUser.Username)
	if err != nil {
		WriteResponse(w, "user not found", http.StatusUnauthorized)
		return
	}
	log.Println(user)
	if user.Password != inUser.Password {
		WriteResponse(w, "invalid password", http.StatusUnauthorized)
		return
	}

	tokenString, err := createTokenForUser(user)
	if err != nil {
		WriteResponse(w, err.Error(), http.StatusBadRequest)
		return
	}

	_ = json.NewEncoder(w).Encode(map[string]interface{}{
		"token": tokenString,
	})
}

func (u *UserHandler) Register(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	body, _ := io.ReadAll(r.Body)
	r.Body.Close()

	user := new(entity.User)
	err := json.Unmarshal(body, user)
	if err != nil {
		WriteResponse(w, err.Error(), http.StatusBadRequest)
		return
	}

	validationErrors := validator.ValidateStruct(user)
	if len(validationErrors) > 0 {
		customErrors := make([]*ApiCustomError, 0, len(validationErrors))
		for _, validationError := range validationErrors {
			log.Println(validationError)
			customErrors = append(customErrors, &ApiCustomError{
				Msg:      validationError.Cause,
				Location: "body",
				Param:    validationError.Field,
				Value:    validationError.Value,
			})
		}
		WriteErrorsResponse(w, customErrors, http.StatusBadRequest)
		return
	}

	_, err = u.UserRepo.GetByName(r.Context(), user.Username)
	if err == nil {
		alreadyExistsError := &ApiCustomError{
			Msg:      "user already exists",
			Location: "body",
			Param:    "username",
			Value:    user.Username,
		}
		WriteErrorsResponse(w, []*ApiCustomError{alreadyExistsError}, http.StatusConflict)
		return
	}

	err = u.UserRepo.Create(r.Context(), user)
	if err != nil {
		WriteResponse(w, err.Error(), http.StatusInternalServerError)
		return
	}
	log.Println(user)

	tokenString, err := createTokenForUser(*user)
	if err != nil {
		WriteResponse(w, err.Error(), http.StatusBadRequest)
		return
	}

	_ = json.NewEncoder(w).Encode(map[string]interface{}{
		"token": tokenString,
	})
}

func createTokenForUser(user entity.User) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, JwtUserClaims{
		User: JwtUser{
			ID:       user.ID,
			Username: user.Username,
		},
		RegisteredClaims: jwt.RegisteredClaims{
			IssuedAt:  jwt.NewNumericDate(time.Now()),
			ExpiresAt: jwt.NewNumericDate(time.Now().Add(config.TokenTTL)),
		},
	})

	tokenString, err := token.SignedString(config.TokenSecret)
	if err != nil {
		return "", err
	}
	return tokenString, nil
}
