package handlers

import (
	"encoding/json"
	"net/http"
)

type ApiResponse struct {
	Message string `json:"message"`
}

type ApiCustomError struct {
	Location string `json:"location"`
	Param    string `json:"param"`
	Value    string `json:"value"`
	Msg      string `json:"msg"`
}

type ApiErrorsResponse struct {
	Errors []*ApiCustomError `json:"errors"`
}

func WriteResponse(w http.ResponseWriter, msg string, status int) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	_ = json.NewEncoder(w).Encode(&ApiResponse{Message: msg})
}

func WriteErrorsResponse(w http.ResponseWriter, errors []*ApiCustomError, status int) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	_ = json.NewEncoder(w).Encode(&ApiErrorsResponse{Errors: errors})
}
