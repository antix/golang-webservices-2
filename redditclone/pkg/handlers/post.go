package handlers

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"redditclone/pkg/entity"
	"redditclone/pkg/repository/comment"
	"redditclone/pkg/repository/post"
	"redditclone/pkg/repository/user"
	"redditclone/pkg/repository/vote"
	"time"

	"github.com/go-playground/validator/v10"

	"github.com/gorilla/mux"
)

var validate *validator.Validate

func init() {
	validate = validator.New(validator.WithRequiredStructEnabled())
}

type userIDKey string

const (
	UserIDContextKey userIDKey = "userID"
)

type PostResponse struct {
	entity.Post
	Author   entity.Author     `json:"author"`
	Votes    []entity.Vote     `json:"votes"`
	Comments []CommentResponse `json:"comments"`
}

// Adds Author object to PostResponse
func (pp *PostResponse) AddAuthor(ctx context.Context, userRepo user.UserStorage) error {
	var err error
	pp.Author, err = user.GetAuthor(ctx, userRepo, pp.AuthorID)
	return err
}

// Adds Votes object to PostResponse
func (pp *PostResponse) AddVotes(ctx context.Context, voteRepo vote.VoteStorage) error {
	var err error
	pp.Votes, err = voteRepo.GetAll(ctx, pp.ID)
	return err
}

// Adds Comments object to PostResponse
func (pp *PostResponse) AddComments(ctx context.Context, commentRepo comment.CommentStorage, userRepo user.UserStorage) error {
	var err error
	comments, _ := commentRepo.GetAll(ctx, pp.ID)
	commentResponses := make([]CommentResponse, 0, len(comments))
	for _, comment := range comments {
		author, _ := user.GetAuthor(ctx, userRepo, comment.AuthorID)
		commentResponses = append(commentResponses, CommentResponse{
			Comment: comment,
			Author:  author,
		})
	}
	pp.Comments = commentResponses
	return err
}

type CommentResponse struct {
	entity.Comment
	Author entity.Author `json:"author"`
}

type PostHandler struct {
	PostRepo    post.PostStorage
	UserRepo    user.UserStorage
	VoteRepo    vote.VoteStorage
	CommentRepo comment.CommentStorage
}

func (p *PostHandler) List(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-type", "application/json")

	posts, err := p.PostRepo.GetAll(r.Context())
	if err != nil {
		WriteResponse(w, err.Error(), http.StatusInternalServerError)
		return
	}

	postResponses := make([]PostResponse, 0, len(posts))
	for _, post := range posts {
		postResponse := PostResponse{
			Post: post,
		}
		// dynamically add Author object to PostResponse
		postResponse.AddAuthor(r.Context(), p.UserRepo)
		// dynamically add Votes object to PostResponse
		postResponse.AddVotes(r.Context(), p.VoteRepo)
		// dynamically add Comments object to PostResponse
		postResponse.AddComments(r.Context(), p.CommentRepo, p.UserRepo)

		postResponses = append(postResponses, postResponse)
	}

	_ = json.NewEncoder(w).Encode(postResponses)
}

func (p *PostHandler) Get(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-type", "application/json")

	vars := mux.Vars(r)
	postID := vars["post_id"]

	post, err := p.PostRepo.GetByID(r.Context(), postID)
	if err != nil {
		WriteResponse(w, err.Error(), http.StatusNotFound)
		return
	}

	post.Views++
	err = p.PostRepo.UpdateViews(r.Context(), postID, post.Views)
	if err != nil {
		WriteResponse(w, err.Error(), http.StatusInternalServerError)
		return
	}

	postResponse := PostResponse{
		Post: post,
	}
	// dynamically add Author object to PostResponse
	postResponse.AddAuthor(r.Context(), p.UserRepo)
	// dynamically add Votes object to PostResponse
	postResponse.AddVotes(r.Context(), p.VoteRepo)
	// dynamically add Comments object to PostResponse
	postResponse.AddComments(r.Context(), p.CommentRepo, p.UserRepo)

	_ = json.NewEncoder(w).Encode(postResponse)
}

func (p *PostHandler) User(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-type", "application/json")

	vars := mux.Vars(r)
	userID := vars["username"]

	user, err := p.UserRepo.GetByName(r.Context(), userID)
	if err != nil {
		WriteResponse(w, err.Error(), http.StatusNotFound)
		return
	}

	posts, err := p.PostRepo.GetByAuthorID(r.Context(), user.ID)
	if err != nil {
		WriteResponse(w, err.Error(), http.StatusNotFound)
		return
	}

	author := entity.Author{Username: user.Username, AuthorID: user.ID}
	postResponses := make([]PostResponse, 0, len(posts))
	for _, post := range posts {
		postResponse := PostResponse{
			Post:   post,
			Author: author, // dynamically add Author object to PostResponse
		}
		// dynamically add Votes object to PostResponse
		postResponse.AddVotes(r.Context(), p.VoteRepo)
		// dynamically add Comments object to PostResponse
		postResponse.AddComments(r.Context(), p.CommentRepo, p.UserRepo)

		postResponses = append(postResponses, postResponse)
	}

	_ = json.NewEncoder(w).Encode(postResponses)
}

func (p *PostHandler) Category(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-type", "application/json")

	vars := mux.Vars(r)
	category := vars["category"]

	posts, err := p.PostRepo.GetByCategory(r.Context(), category)
	if err != nil {
		WriteResponse(w, err.Error(), http.StatusNotFound)
		return
	}

	postResponses := make([]PostResponse, 0, len(posts))
	for _, post := range posts {
		postResponse := PostResponse{
			Post: post,
		}
		// dynamically add Author object to PostResponse
		postResponse.AddAuthor(r.Context(), p.UserRepo)
		// dynamically add Votes object to PostResponse
		postResponse.AddVotes(r.Context(), p.VoteRepo)
		// dynamically add Comments object to PostResponse
		postResponse.AddComments(r.Context(), p.CommentRepo, p.UserRepo)

		postResponses = append(postResponses, postResponse)
	}

	_ = json.NewEncoder(w).Encode(postResponses)
}

func (p *PostHandler) Create(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-type", "application/json")
	body, _ := io.ReadAll(r.Body)
	r.Body.Close()

	userID, ok := r.Context().Value(UserIDContextKey).(string)
	if !ok {
		WriteResponse(w, "bad userID", http.StatusUnauthorized)
		return
	}
	author, err := user.GetAuthor(r.Context(), p.UserRepo, userID)
	if err != nil {
		WriteResponse(w, err.Error(), http.StatusUnauthorized)
		return
	}

	post := new(entity.Post)
	err = json.Unmarshal(body, post)
	if err != nil {
		WriteResponse(w, err.Error(), http.StatusBadRequest)
		return
	}

	err = validate.Struct(post)
	if err != nil {
		var validateErrs validator.ValidationErrors
		if errors.As(err, &validateErrs) {
			customErrors := make([]*ApiCustomError, 0, len(validateErrs))
			for _, e := range validateErrs {
				log.Println(e)
				customErrors = append(customErrors, &ApiCustomError{
					Msg:      e.Error(),
					Location: "body",
					Param:    e.Field(),
					Value:    fmt.Sprint(e.Value()),
				})
			}
			WriteErrorsResponse(w, customErrors, http.StatusBadRequest)
			return
		}
	}

	post.AuthorID = author.AuthorID
	post.Score = 1
	vote := entity.Vote{AuthorID: author.AuthorID, Vote: 1}
	post.CalculateUpvotePercentage([]entity.Vote{vote})
	post.Created = time.Now()

	err = p.PostRepo.Create(r.Context(), post)
	if err != nil {
		WriteResponse(w, err.Error(), http.StatusInternalServerError)
		return
	}

	err = p.VoteRepo.Add(r.Context(), post.ID, vote)
	if err != nil {
		WriteResponse(w, err.Error(), http.StatusInternalServerError)
		return
	}

	_ = json.NewEncoder(w).Encode(PostResponse{
		Post:     *post,
		Author:   author,
		Votes:    []entity.Vote{vote},
		Comments: []CommentResponse{},
	})
}

func (p *PostHandler) Remove(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-type", "application/json")

	vars := mux.Vars(r)
	postID := vars["post_id"]

	userID, ok := r.Context().Value(UserIDContextKey).(string)
	if !ok {
		WriteResponse(w, "bad userID", http.StatusUnauthorized)
		return
	}

	post, err := p.PostRepo.GetByID(r.Context(), postID)
	if err != nil {
		WriteResponse(w, err.Error(), http.StatusNotFound)
		return
	}

	if post.AuthorID != userID {
		WriteResponse(w, "not the author", http.StatusUnauthorized)
		return
	}

	err = p.PostRepo.Remove(r.Context(), postID)
	if err != nil {
		WriteResponse(w, err.Error(), http.StatusNotFound)
		return
	}

	p.VoteRepo.RemoveAll(r.Context(), postID)
	p.CommentRepo.RemoveAll(r.Context(), postID)

	WriteResponse(w, "success", http.StatusOK)
}

func (p *PostHandler) Vote(vote entity.VoteValue) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-type", "application/json")

		vars := mux.Vars(r)
		postID := vars["post_id"]

		userID, ok := r.Context().Value(UserIDContextKey).(string)
		if !ok {
			WriteResponse(w, "bad userID", http.StatusUnauthorized)
			return
		}

		post, err := p.PostRepo.GetByID(r.Context(), postID)
		if err != nil {
			WriteResponse(w, err.Error(), http.StatusNotFound)
			return
		}

		existingVote, err := p.VoteRepo.GetByAuthorID(r.Context(), postID, userID)
		if err == nil {
			if vote == entity.Unvote { // Remove existingVote
				post.Score -= int(existingVote.Vote)

				err = p.VoteRepo.RemoveByAuthorID(r.Context(), postID, userID)
				if err != nil {
					WriteResponse(w, err.Error(), http.StatusInternalServerError)
					return
				}
			} else if vote != existingVote.Vote { // Update existingVote
				post.Score -= int(existingVote.Vote)
				post.Score += int(vote)

				err = p.VoteRepo.UpdateByAuthorID(r.Context(), postID, userID, vote)
				if err != nil {
					WriteResponse(w, err.Error(), http.StatusInternalServerError)
					return
				}
			} else if vote == existingVote.Vote { // Do nothing
				WriteResponse(w, "vote already exists", http.StatusConflict)
				return
			}
		} else if err == entity.ErrVoteNotFound && vote != entity.Unvote { // Add new vote
			post.Score += int(vote)

			err = p.VoteRepo.Add(r.Context(), postID, entity.Vote{AuthorID: userID, Vote: vote})
			if err != nil {
				WriteResponse(w, err.Error(), http.StatusInternalServerError)
				return
			}

		}

		votes, err := p.VoteRepo.GetAll(r.Context(), postID)
		if err != nil {
			WriteResponse(w, err.Error(), http.StatusInternalServerError)
			return
		}
		post.CalculateUpvotePercentage(votes)
		p.PostRepo.Update(r.Context(), post)

		postResponse := PostResponse{
			Post:  post,
			Votes: votes, // dynamically add Votes object to PostResponse
		}
		// dynamically add Author object to PostResponse
		postResponse.AddAuthor(r.Context(), p.UserRepo)
		// dynamically add Comments object to PostResponse
		postResponse.AddComments(r.Context(), p.CommentRepo, p.UserRepo)

		_ = json.NewEncoder(w).Encode(postResponse)
	}
}

func (p *PostHandler) RemoveComment(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-type", "application/json")

	vars := mux.Vars(r)
	postID := vars["post_id"]
	commentID := vars["comment_id"]

	userID, ok := r.Context().Value(UserIDContextKey).(string)
	if !ok {
		WriteResponse(w, "bad userID", http.StatusUnauthorized)
		return
	}

	post, err := p.PostRepo.GetByID(r.Context(), postID)
	if err != nil {
		WriteResponse(w, err.Error(), http.StatusNotFound)
		return
	}

	comment, err := p.CommentRepo.GetByID(r.Context(), postID, commentID)
	if err != nil {
		WriteResponse(w, err.Error(), http.StatusNotFound)
		return
	}

	if comment.AuthorID != userID && post.AuthorID != userID {
		WriteResponse(w, "not the comment or post author", http.StatusUnauthorized)
		return
	}

	err = p.CommentRepo.Remove(r.Context(), postID, commentID)
	if err != nil {
		WriteResponse(w, err.Error(), http.StatusInternalServerError)
		return
	}

	postResponse := PostResponse{
		Post: post,
	}
	// dynamically add Author object to PostResponse
	postResponse.AddAuthor(r.Context(), p.UserRepo)
	// dynamically add Votes object to PostResponse
	postResponse.AddVotes(r.Context(), p.VoteRepo)
	// dynamically add Comments object to PostResponse
	postResponse.AddComments(r.Context(), p.CommentRepo, p.UserRepo)

	_ = json.NewEncoder(w).Encode(postResponse)
}

func (p *PostHandler) AddComment(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-type", "application/json")
	body, _ := io.ReadAll(r.Body)
	r.Body.Close()

	vars := mux.Vars(r)
	postID := vars["post_id"]

	userID, ok := r.Context().Value(UserIDContextKey).(string)
	if !ok {
		WriteResponse(w, "bad userID", http.StatusUnauthorized)
		return
	}

	post, err := p.PostRepo.GetByID(r.Context(), postID)
	if err != nil {
		WriteResponse(w, err.Error(), http.StatusNotFound)
		return
	}

	incomingComment := new(entity.IncomingComment)
	err = json.Unmarshal(body, incomingComment)
	if err != nil {
		WriteResponse(w, err.Error(), http.StatusBadRequest)
		return
	}

	err = validate.Struct(incomingComment)
	if err != nil {
		var validateErrs validator.ValidationErrors
		if errors.As(err, &validateErrs) {
			customErrors := make([]*ApiCustomError, 0, len(validateErrs))
			for _, e := range validateErrs {
				log.Println(e)
				customErrors = append(customErrors, &ApiCustomError{
					Msg:      e.Error(),
					Location: "body",
					Param:    e.Field(),
					Value:    fmt.Sprint(e.Value()),
				})
			}
			WriteErrorsResponse(w, customErrors, http.StatusBadRequest)
			return
		}
	}

	comment := entity.Comment{
		AuthorID: userID,
		Created:  time.Now(),
		Body:     incomingComment.Comment,
	}

	err = p.CommentRepo.Add(r.Context(), postID, &comment)
	if err != nil {
		WriteResponse(w, err.Error(), http.StatusInternalServerError)
		return
	}

	postResponse := PostResponse{
		Post: post,
	}
	// dynamically add Author object to PostResponse
	postResponse.AddAuthor(r.Context(), p.UserRepo)
	// dynamically add Votes object to PostResponse
	postResponse.AddVotes(r.Context(), p.VoteRepo)
	// dynamically add Comments object to PostResponse
	postResponse.AddComments(r.Context(), p.CommentRepo, p.UserRepo)

	_ = json.NewEncoder(w).Encode(postResponse)
}
