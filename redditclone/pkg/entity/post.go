package entity

import (
	"errors"
	"math"
	"time"
)

const (
	Downvote VoteValue = iota - 1
	Unvote
	Upvote
)

var (
	ErrPostNotFound    = errors.New("post not found")
	ErrCommentNotFound = errors.New("comment not found")
	ErrVoteNotFound    = errors.New("vote not found")
)

type VoteValue int8

type Post struct {
	ID               string    `json:"id"`
	Type             string    `json:"type" validate:"required,oneof=text link"`
	Title            string    `json:"title" validate:"required,gte=1,lte=100"`
	Text             string    `json:"text,omitempty" validate:"required_if=Type text,omitempty,gte=4"`
	URL              string    `json:"url,omitempty" validate:"required_if=Type link,omitempty,url"`
	Category         string    `json:"category" validate:"required"`
	Score            int       `json:"score"`
	Views            int       `json:"views"`
	Created          time.Time `json:"created"`
	UpvotePercentage int       `json:"upvotePercentage"`
	AuthorID         string    `json:"authorID"` //TODO: set to `-`` after deletion of exampleData
}

type Author struct {
	AuthorID string `json:"id"`
	Username string `json:"username"`
}

type Comment struct {
	ID       string    `json:"id"`
	Body     string    `json:"body"`
	Created  time.Time `json:"created"`
	AuthorID string    `json:"authorID"` //TODO: set to `-`` after deletion of exampleData
}

type IncomingComment struct {
	Comment string `json:"comment" validate:"required,gte=1,lte=2000"`
}

type Vote struct {
	AuthorID string    `json:"user"`
	Vote     VoteValue `json:"vote"`
}

// CalculateUpvotePercentage calculates the percentage of upvotes
func (p *Post) CalculateUpvotePercentage(votes []Vote) {
	if len(votes) > 0 {
		upvotes := 0
		for _, vote := range votes {
			if vote.Vote == Upvote {
				upvotes++
			}
		}
		p.UpvotePercentage = int(math.Round(float64(upvotes) / float64(len(votes)) * 100))
	} else {
		p.UpvotePercentage = 0
	}
}
