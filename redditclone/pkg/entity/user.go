package entity

type User struct {
	ID       string `json:"id"`
	Username string `json:"username" validator:"required,min=1,max=32,regexp=^[a-zA-Z0-9_-]+$"`
	Password string `json:"password" validator:"required,min=8,max=72"`
}
