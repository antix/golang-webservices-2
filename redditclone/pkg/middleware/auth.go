package middleware

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"redditclone/pkg/config"
	"redditclone/pkg/handlers"
	"strings"

	"github.com/golang-jwt/jwt/v5"
)

func Auth(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		tokenString := strings.TrimPrefix(r.Header.Get("Authorization"), "Bearer ")

		hashSecretGetter := func(token *jwt.Token) (interface{}, error) {
			method, ok := token.Method.(*jwt.SigningMethodHMAC)
			if !ok || method.Alg() != "HS256" {
				return nil, fmt.Errorf("bad signing method")
			}
			return config.TokenSecret, nil
		}

		token, err := jwt.ParseWithClaims(tokenString, &handlers.JwtUserClaims{}, hashSecretGetter)
		if err != nil {
			handlers.WriteResponse(w, err.Error(), http.StatusUnauthorized)
			return
		}
		if !token.Valid {
			handlers.WriteResponse(w, "invalid token", http.StatusUnauthorized)
			return
		}

		payload, ok := token.Claims.(*handlers.JwtUserClaims)
		if !ok {
			handlers.WriteResponse(w, "no payload", http.StatusUnauthorized)
			return
		}

		log.Printf("JWT: %+v", payload)
		ctx := context.WithValue(r.Context(), handlers.UserIDContextKey, payload.User.ID)
		r = r.WithContext(ctx)

		next.ServeHTTP(w, r)
	})
}
