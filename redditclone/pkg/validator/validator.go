package validator

import (
	"fmt"
	"log"
	"reflect"
	"regexp"
	"strconv"
	"strings"
    "net/url"
)

type ValidationError struct {
	Field string
	Value string
	Cause string
}

func IsUrl(str string) bool {
	u, err := url.Parse(str)
	return err == nil && u.Scheme != "" && u.Host != ""
}

func ValidateStruct(u interface{}) []*ValidationError {
	val := reflect.ValueOf(u).Elem()
	var validationErrors []*ValidationError

	//fmt.Printf("%T have %d fields:\n", u, val.NumField())
	for i := 0; i < val.NumField(); i++ {
		valueField := val.Field(i)
		typeField := val.Type().Field(i)

		//fmt.Printf("\tname=%v, type=%v, value=%v, tag=`%v`\n", typeField.Name, typeField.Type.Kind(), valueField, typeField.Tag)

		if typeField.Tag != "" {
			tagValue := typeField.Tag.Get("validator")
			if tagValue == "-" || tagValue == "" {
				continue
			}

			// support only int and string
			if typeField.Type.Kind() != reflect.String && typeField.Type.Kind() != reflect.Int {
				continue
			}

			tagValues := strings.Split(tagValue, ",")
			var validatorName, validatorValue string
			for _, tagValue := range tagValues {
				temp := strings.Split(tagValue, "=")
				if len(temp) > 0 {
					validatorName = temp[0]
				}
				if len(temp) > 1 {
					validatorValue = temp[1]
				}

				switch validatorName {
				case "default":
					if valueField.IsZero() {
						switch typeField.Type.Kind() {
						case reflect.String:
							valueField.Set(reflect.ValueOf(&validatorValue).Elem())
						case reflect.Int:
							defaultValue, _ := strconv.Atoi(validatorValue)
							valueField.Set(reflect.ValueOf(&defaultValue).Elem())
						}
					}
				case "required":
					if valueField.IsZero() {
						validationErrors = append(validationErrors, &ValidationError{
							Cause: fmt.Sprintf("%s is required", typeField.Name),
							Field: typeField.Name,
							Value: fmt.Sprintf("%v", valueField),
						})
					}
				case "min":
					minValue, _ := strconv.Atoi(validatorValue)
					switch typeField.Type.Kind() {
					case reflect.String:
						if len(valueField.String()) < minValue {
							validationErrors = append(validationErrors, &ValidationError{
								Cause: fmt.Sprintf("%s must be at least %d characters long", typeField.Name, minValue),
								Field: typeField.Name,
								Value: valueField.String(),
							})
						}
					case reflect.Int:
						if valueField.Int() < int64(minValue) {
							validationErrors = append(validationErrors, &ValidationError{
								Cause: fmt.Sprintf("%s must be >= %d", typeField.Name, minValue),
								Field: typeField.Name,
								Value: fmt.Sprintf("%v", valueField),
							})
						}
					}
				case "max":
					maxValue, _ := strconv.Atoi(validatorValue)
					switch typeField.Type.Kind() {
					case reflect.String:
						if len(valueField.String()) > maxValue {
							validationErrors = append(validationErrors, &ValidationError{
								Cause: fmt.Sprintf("%s must be at most %d characters long", typeField.Name, maxValue),
								Field: typeField.Name,
								Value: valueField.String(),
							})
						}
					case reflect.Int:
						if valueField.Int() > int64(maxValue) {
							validationErrors = append(validationErrors, &ValidationError{
								Cause: fmt.Sprintf("%s must be <= %d", typeField.Name, maxValue),
								Field: typeField.Name,
								Value: fmt.Sprintf("%v", valueField),
							})
						}
					}
				case "regexp":
					switch typeField.Type.Kind() {
					case reflect.String:
						validatorRegexp, err := regexp.Compile(validatorValue)
						if err != nil {
							log.Println("Bad validator regexp:", err)
							continue
						}
						if !validatorRegexp.MatchString(valueField.String()) {
							validationErrors = append(validationErrors, &ValidationError{
								Cause: fmt.Sprintf("%s must match regexp '%v'", typeField.Name, validatorValue),
								Field: typeField.Name,
								Value: valueField.String(),
							})
						}
					}
				}
			}
		}
	}

	return validationErrors
}
