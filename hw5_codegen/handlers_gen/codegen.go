package main

// код писать тут

import (
	"encoding/json"
	"fmt"
	"go/ast"
	"go/parser"
	"go/token"
	"log"
	"os"
	"reflect"
	"strconv"
	"strings"
	"text/template"
)

type APIStructTypeName string
type ValidateStructTypeName string

type APIHandler struct {
	FunctionName             string
	Parameters               APIHandlerParams
	FunctionArgumentTypeName ValidateStructTypeName
	FunctionResultTypeName   string
}

type APIHandlerParams struct {
	URL    string `json:"url"`
	Auth   bool   `json:"auth"`
	Method string `json:"method"`
}

type StructFieldValidator struct {
	FieldName  string
	FieldType  string
	Validators map[string]interface{}
}

type TemplateParameters struct {
	APIStructs      map[APIStructTypeName][]APIHandler
	ValidateStructs map[ValidateStructTypeName][]StructFieldValidator
}

func main() {
	fset := token.NewFileSet()
	node, err := parser.ParseFile(fset, os.Args[1], nil, parser.ParseComments)
	if err != nil {
		log.Fatal(err)
	}

	out, err := os.Create(os.Args[2])
	if err != nil {
		log.Fatal(err)
	}
	//out := os.Stdout

	templateParameters := TemplateParameters{
		APIStructs:      make(map[APIStructTypeName][]APIHandler),
		ValidateStructs: make(map[ValidateStructTypeName][]StructFieldValidator),
	}

	for _, f := range node.Decls {
		switch f := f.(type) {
		case *ast.GenDecl:
			for _, spec := range f.Specs {
				currType, ok := spec.(*ast.TypeSpec)
				if !ok {
					//log.Printf("SKIP %T is not ast.TypeSpec\n", spec)
					continue
				}
				currStruct, ok := currType.Type.(*ast.StructType)
				if !ok {
					//log.Printf("SKIP %T is not ast.StructType\n", currStruct)
					continue
				}
				//message := fmt.Sprintf("Generating code for struct %q\n", currType.Name.Name)
				//shouldGenerate := false
				for _, field := range currStruct.Fields.List {
					if field.Tag != nil {
						tag := reflect.StructTag(field.Tag.Value[1 : len(field.Tag.Value)-1])
						tagValue := tag.Get("apivalidator")
						if tagValue == "-" || tagValue == "" {
							continue
						}
						//shouldGenerate = true

						fieldName := field.Names[0].Name
						var fieldType string
						// support only int and string
						switch t := field.Type.(type) {
						case *ast.Ident:
							fieldType = t.Name
							if fieldType != "int" && fieldType != "string" {
								continue
							}
						default:
							continue
						}
						//fieldType := field.Type.(*ast.Ident).Name
						tagValues := strings.Split(tagValue, ",")
						validatorsMap := make(map[string]interface{})
						var validatorName, validatorValue string
						for _, tagValue := range tagValues {
							temp := strings.Split(tagValue, "=")
							if len(temp) > 0 {
								validatorName = temp[0]
							}
							if len(temp) > 1 {
								validatorValue = temp[1]
							}

							//validatorsMap["paramname"] = strings.ToLower(fieldName)
							//validatorsMap["required"] = false

							switch validatorName {
							case "default":
								if fieldType == "string" {
									validatorsMap[validatorName] = validatorValue
								} else if fieldType == "int" {
									defaultValue, _ := strconv.Atoi(validatorValue)
									validatorsMap[validatorName] = defaultValue
								}
							case "required":
								validatorsMap[validatorName] = true
							case "paramname":
								validatorsMap[validatorName] = validatorValue
							case "min":
								minValue, _ := strconv.Atoi(validatorValue)
								validatorsMap[validatorName] = minValue
							case "max":
								maxValue, _ := strconv.Atoi(validatorValue)
								validatorsMap[validatorName] = maxValue
							case "enum":
								enums := strings.Split(validatorValue, "|")
								if fieldType == "string" {
									validatorsMap[validatorName] = enums
								} else if fieldType == "int" {
									intEnums := make([]int, len(enums))
									for i, enum := range enums {
										intEnum, _ := strconv.Atoi(enum)
										intEnums[i] = intEnum
									}
									validatorsMap[validatorName] = intEnums
								}
							}
						}

						//message += fmt.Sprintf("\tGenerating code for field %q of type %q with validators %q\n", fieldName, fieldType, validatorsMap)

						validateStructTypeName := ValidateStructTypeName(currType.Name.Name)
						templateParameters.ValidateStructs[validateStructTypeName] = append(templateParameters.ValidateStructs[validateStructTypeName], StructFieldValidator{
							FieldName:  fieldName,
							FieldType:  fieldType,
							Validators: validatorsMap,
						})
					}
				}
				/*if shouldGenerate {
					fmt.Println(message)
				}*/

			}
		case *ast.FuncDecl:
			doc := f.Doc.Text()
			if strings.HasPrefix(doc, "apigen:api ") {
				doc := strings.TrimPrefix(doc, "apigen:api ")
				var params APIHandlerParams
				err := json.Unmarshal([]byte(doc), &params)
				if err != nil {
					log.Printf("Error parsing HandlerParams: %s", err)
					continue
				}
				/*for _, funcArg := range f.Type.Params.List {
					PrintType(funcArg.Type)
				}*/
				funcParam := f.Type.Params.List[1].Type.(*ast.Ident).Name
				funcResult := f.Type.Results.List[0].Type.(*ast.StarExpr).X.(*ast.Ident).Name
				handler := APIHandler{
					FunctionName:             f.Name.Name,
					Parameters:               params,
					FunctionArgumentTypeName: ValidateStructTypeName(funcParam),
					FunctionResultTypeName:   funcResult,
				}
				funcReceiver := APIStructTypeName(f.Recv.List[0].Type.(*ast.StarExpr).X.(*ast.Ident).Name)
				//log.Printf("Method %q for %q has params '%+v'\n", f.Name.Name, funcReceiver, params)
				templateParameters.APIStructs[funcReceiver] = append(templateParameters.APIStructs[funcReceiver], handler)
			} else {
				continue
			}
		default:
			log.Printf("SKIP %T\n", f)
			continue
		}
	}

	header := `package main

import (
	"encoding/json"
	"net/http"
	"strconv"
)
`

	serveHTTPTemplate := template.Must(template.New("serveHTTPTemplate").Parse(`
type Response struct {
	Error    string      ` + "`" + `json:"error"` + "`" + `
	Response interface{} ` + "`" + `json:"response,omitempty"` + "`" + `
}

{{ range $structName, $handlers := .APIStructs -}}
{{ range $handlers -}}
type {{$structName}}{{.FunctionName}}Response struct {
	Error    string ` + "`" + `json:"error"` + "`" + `
	Response *{{.FunctionResultTypeName}} ` + "`" + `json:"response,omitempty"` + "`" + `
}
{{end}}
{{end}}

func wrapperDefault(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusNotFound)
	_ = json.NewEncoder(w).Encode(Response{
		Error: "unknown method",
	})
}

{{ range $structName, $handlers := .APIStructs -}}
func (h *{{$structName}}) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	switch r.URL.Path {
	{{- range $handlers }}
	case "{{.Parameters.URL}}":
		h.wrapper{{.FunctionName}}(w, r)
	{{- end}}
	default:
		wrapperDefault(w, r)
	}
}
{{end}}`))

	funcMap := template.FuncMap{
		"ToLower": strings.ToLower,
		"JoinInts": func(elems []int, sep string) string {
			return strings.Join(strings.Fields(strings.Trim(fmt.Sprint(elems), "[]")), sep)
		},
		"JoinStrings": strings.Join,
	}
	wrapperTemplate := template.Must(template.New("wrapperTemplate").Funcs(funcMap).Parse(`
{{ $validateStructs := .ValidateStructs -}}
{{ range $structName, $handlers := .APIStructs -}}
{{ range $handlers -}}
func (h *{{$structName}}) wrapper{{.FunctionName}}(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	{{ if ne .Parameters.Method "" -}}
	//check HTTP method
	if r.Method != "{{.Parameters.Method}}" {
		w.WriteHeader(http.StatusNotAcceptable)
		_ = json.NewEncoder(w).Encode(Response{
			Error: "bad method",
		})
		return
	}
	{{end -}}

	{{ if .Parameters.Auth -}}
	//check auth header "X-Auth": "100500"
	if r.Header.Get("X-Auth") != "100500" {
		w.WriteHeader(http.StatusForbidden)
		_ = json.NewEncoder(w).Encode(Response{
			Error: "unauthorized",
		})
		return
	}
	{{end -}}

	//parse parameters
	err := r.ParseForm()
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		_ = json.NewEncoder(w).Encode(Response{
			Error: err.Error(),
		})
		return
	}
	// валидирование параметров
	var params {{.FunctionArgumentTypeName}}

	{{ $structFieldValidators := index $validateStructs .FunctionArgumentTypeName -}}
	{{ range $structFieldValidators }}
	{ // each field has own block
		{{ $paramname := .FieldName | ToLower -}}
		{{ if index .Validators "paramname" -}}
		{{ $paramname = index .Validators "paramname" -}}
		{{end -}}

		{{ if index .Validators "required" -}}
		if !r.Form.Has("{{$paramname}}") {
			w.WriteHeader(http.StatusBadRequest)
			_ = json.NewEncoder(w).Encode(Response{
				Error: "{{$paramname}} must me not empty",
			})
			return
		}
		{{end -}}

		var value {{.FieldType}}
		stringValue := r.Form.Get("{{$paramname}}")
		if stringValue != "" {
		{{- if eq .FieldType "string" }}
			value = stringValue
		{{- end}}
		{{- if eq .FieldType "int" }}
			value, err = strconv.Atoi(stringValue)
			if err != nil {
				w.WriteHeader(http.StatusBadRequest)
				_ = json.NewEncoder(w).Encode(Response{
					Error: "{{$paramname}} must be int",
				})
				return
			}
		{{- end}}
		{{- /* "if" treats nil and 0 as false, but 0 is valid value for "default", so we can't use "index" in "if" */ -}}
		{{- $default := index .Validators "default" }}
		{{- if ne $default nil }}
		} else {
			{{- if eq .FieldType "int" }}
			value = {{ index .Validators "default" }}
			{{- end}}
			{{- if eq .FieldType "string" }}
			value = "{{ index .Validators "default" }}"
			{{- end}}
		{{- end}}
		}

		{{/* "if" treats nil and 0 as false, but 0 is valid value for "min", so we can't use "index" in "if" */ -}}
		{{ $min := index .Validators "min" -}}
		{{ if ne $min nil -}}
		{{- if eq .FieldType "string" }}
		if len(value) < {{$min}} {
			w.WriteHeader(http.StatusBadRequest)
			_ = json.NewEncoder(w).Encode(Response{
				Error: "{{$paramname}} len must be >= {{$min}}",
			})
			return
		}
		{{end -}}
		{{- if eq .FieldType "int" }}
		if value < {{$min}} {
			w.WriteHeader(http.StatusBadRequest)
			_ = json.NewEncoder(w).Encode(Response{
				Error: "{{$paramname}} must be >= {{$min}}",
			})
			return
		}
		{{end -}}
		{{end -}}

		{{ $max := index .Validators "max" -}}
		{{ if ne $max nil -}}
		{{- if eq .FieldType "string" }}
		if len(value) > {{$max}} {
			w.WriteHeader(http.StatusBadRequest)
			_ = json.NewEncoder(w).Encode(Response{
				Error: "{{$paramname}} len must be <= {{$max}}",
			})
			return
		}
		{{end -}}
		{{- if eq .FieldType "int" }}
		if value > {{$max}} {
			w.WriteHeader(http.StatusBadRequest)
			_ = json.NewEncoder(w).Encode(Response{
				Error: "{{$paramname}} must be <= {{$max}}",
			})
			return
		}
		{{end -}}
		{{end -}}

		{{- if index .Validators "enum" }}
		{{ $enum := index .Validators "enum" -}}
		enumValues := {{ printf "%#v" $enum }}
		contains := false
		for _, enumValue := range enumValues {
			if enumValue == value {
				contains = true
				break
			}
		}
		if !contains {
			{{- $enumList := "" -}}
			{{- if eq .FieldType "string" }}
			{{ $enumList = JoinStrings $enum ", " -}}
			{{end -}}
			{{- if eq .FieldType "int" }}
			{{ $enumList = JoinInts $enum ", " -}}
			{{end -}}
			w.WriteHeader(http.StatusBadRequest)
			_ = json.NewEncoder(w).Encode(Response{
				Error: "{{$paramname}} must be one of [{{ $enumList }}]",
			})
			return
		}
		{{end -}}

		params.{{.FieldName}} = value
	} // end of field block
	{{end}}

	// вызов оригинального метода
	res, err := h.{{.FunctionName}}(r.Context(), params)
	// прочие обработки
	response := {{$structName}}{{.FunctionName}}Response{
		Response: res,
	}
	{{/* we can use generic Response struct with interface{} instead of specific structs */ -}}
	{{/* response := Response{} */ -}}
	if err != nil {
		response.Error = err.Error()
		if err, ok := err.(ApiError); ok {
			w.WriteHeader(err.HTTPStatus)
		} else {
			w.WriteHeader(http.StatusInternalServerError)
		}
	}{{/* else {
		response.Response = res
		w.WriteHeader(http.StatusOK)
	} */}}

	_ = json.NewEncoder(w).Encode(response)
}
{{end}}
{{end}}`))

	for structName, handlers := range templateParameters.APIStructs {
		for _, handler := range handlers {
			log.Printf("%q: generating method %q with API parameters '%+v' and with validation for %q and it returns %q\n", structName, handler.FunctionName, handler.Parameters, handler.FunctionArgumentTypeName, handler.FunctionResultTypeName)
		}
	}
	for structName, validators := range templateParameters.ValidateStructs {
		for _, validator := range validators {
			log.Printf("%q: generating validator for field %q of type %q with validators %v\n", structName, validator.FieldName, validator.FieldType, validator.Validators)
		}
	}
	out.WriteString(header)
	err = serveHTTPTemplate.Execute(out, templateParameters)
	if err != nil {
		log.Fatal(err)
	}
	err = wrapperTemplate.Execute(out, templateParameters)
	if err != nil {
		log.Fatal(err)
	}
}

/*func PrintType(funcArgType ast.Expr) {
	switch tyExpr := funcArgType.(type) {
	case *ast.Ident:
		fmt.Println("ast.Ident")
		fmt.Println(tyExpr.Name)
	case *ast.SelectorExpr:
		fmt.Println("ast.SelectorExpr")
		PrintType(tyExpr.X)
	case *ast.StarExpr:
		fmt.Println("ast.StarExpr")
		PrintType(tyExpr.X)
	default:
		fmt.Println("default")
		fmt.Printf("%#v\n", tyExpr)
	}
}*/
