package main

import (
	"encoding/json"
	"net/http"
	"strconv"
)

type Response struct {
	Error    string      `json:"error"`
	Response interface{} `json:"response,omitempty"`
}

type MyApiProfileResponse struct {
	Error    string `json:"error"`
	Response *User  `json:"response,omitempty"`
}
type MyApiCreateResponse struct {
	Error    string   `json:"error"`
	Response *NewUser `json:"response,omitempty"`
}

type OtherApiCreateResponse struct {
	Error    string     `json:"error"`
	Response *OtherUser `json:"response,omitempty"`
}

func wrapperDefault(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusNotFound)
	_ = json.NewEncoder(w).Encode(Response{
		Error: "unknown method",
	})
}

func (h *MyApi) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	switch r.URL.Path {
	case "/user/profile":
		h.wrapperProfile(w, r)
	case "/user/create":
		h.wrapperCreate(w, r)
	default:
		wrapperDefault(w, r)
	}
}
func (h *OtherApi) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	switch r.URL.Path {
	case "/user/create":
		h.wrapperCreate(w, r)
	default:
		wrapperDefault(w, r)
	}
}

func (h *MyApi) wrapperProfile(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	//parse parameters
	err := r.ParseForm()
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		_ = json.NewEncoder(w).Encode(Response{
			Error: err.Error(),
		})
		return
	}
	// валидирование параметров
	var params ProfileParams

	{ // each field has own block
		if !r.Form.Has("login") {
			w.WriteHeader(http.StatusBadRequest)
			_ = json.NewEncoder(w).Encode(Response{
				Error: "login must me not empty",
			})
			return
		}
		var value string
		stringValue := r.Form.Get("login")
		if stringValue != "" {
			value = stringValue
		}

		params.Login = value
	} // end of field block

	// вызов оригинального метода
	res, err := h.Profile(r.Context(), params)
	// прочие обработки
	response := MyApiProfileResponse{
		Response: res,
	}
	if err != nil {
		response.Error = err.Error()
		if err, ok := err.(ApiError); ok {
			w.WriteHeader(err.HTTPStatus)
		} else {
			w.WriteHeader(http.StatusInternalServerError)
		}
	}

	_ = json.NewEncoder(w).Encode(response)
}
func (h *MyApi) wrapperCreate(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	//check HTTP method
	if r.Method != "POST" {
		w.WriteHeader(http.StatusNotAcceptable)
		_ = json.NewEncoder(w).Encode(Response{
			Error: "bad method",
		})
		return
	}
	//check auth header "X-Auth": "100500"
	if r.Header.Get("X-Auth") != "100500" {
		w.WriteHeader(http.StatusForbidden)
		_ = json.NewEncoder(w).Encode(Response{
			Error: "unauthorized",
		})
		return
	}
	//parse parameters
	err := r.ParseForm()
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		_ = json.NewEncoder(w).Encode(Response{
			Error: err.Error(),
		})
		return
	}
	// валидирование параметров
	var params CreateParams

	{ // each field has own block
		if !r.Form.Has("login") {
			w.WriteHeader(http.StatusBadRequest)
			_ = json.NewEncoder(w).Encode(Response{
				Error: "login must me not empty",
			})
			return
		}
		var value string
		stringValue := r.Form.Get("login")
		if stringValue != "" {
			value = stringValue
		}

		if len(value) < 10 {
			w.WriteHeader(http.StatusBadRequest)
			_ = json.NewEncoder(w).Encode(Response{
				Error: "login len must be >= 10",
			})
			return
		}
		params.Login = value
	} // end of field block

	{ // each field has own block
		var value string
		stringValue := r.Form.Get("full_name")
		if stringValue != "" {
			value = stringValue
		}

		params.Name = value
	} // end of field block

	{ // each field has own block
		var value string
		stringValue := r.Form.Get("status")
		if stringValue != "" {
			value = stringValue
		} else {
			value = "user"
		}

		enumValues := []string{"user", "moderator", "admin"}
		contains := false
		for _, enumValue := range enumValues {
			if enumValue == value {
				contains = true
				break
			}
		}
		if !contains {
			w.WriteHeader(http.StatusBadRequest)
			_ = json.NewEncoder(w).Encode(Response{
				Error: "status must be one of [user, moderator, admin]",
			})
			return
		}
		params.Status = value
	} // end of field block

	{ // each field has own block
		var value int
		stringValue := r.Form.Get("age")
		if stringValue != "" {
			value, err = strconv.Atoi(stringValue)
			if err != nil {
				w.WriteHeader(http.StatusBadRequest)
				_ = json.NewEncoder(w).Encode(Response{
					Error: "age must be int",
				})
				return
			}
		}

		if value < 0 {
			w.WriteHeader(http.StatusBadRequest)
			_ = json.NewEncoder(w).Encode(Response{
				Error: "age must be >= 0",
			})
			return
		}

		if value > 128 {
			w.WriteHeader(http.StatusBadRequest)
			_ = json.NewEncoder(w).Encode(Response{
				Error: "age must be <= 128",
			})
			return
		}
		params.Age = value
	} // end of field block

	// вызов оригинального метода
	res, err := h.Create(r.Context(), params)
	// прочие обработки
	response := MyApiCreateResponse{
		Response: res,
	}
	if err != nil {
		response.Error = err.Error()
		if err, ok := err.(ApiError); ok {
			w.WriteHeader(err.HTTPStatus)
		} else {
			w.WriteHeader(http.StatusInternalServerError)
		}
	}

	_ = json.NewEncoder(w).Encode(response)
}

func (h *OtherApi) wrapperCreate(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	//check HTTP method
	if r.Method != "POST" {
		w.WriteHeader(http.StatusNotAcceptable)
		_ = json.NewEncoder(w).Encode(Response{
			Error: "bad method",
		})
		return
	}
	//check auth header "X-Auth": "100500"
	if r.Header.Get("X-Auth") != "100500" {
		w.WriteHeader(http.StatusForbidden)
		_ = json.NewEncoder(w).Encode(Response{
			Error: "unauthorized",
		})
		return
	}
	//parse parameters
	err := r.ParseForm()
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		_ = json.NewEncoder(w).Encode(Response{
			Error: err.Error(),
		})
		return
	}
	// валидирование параметров
	var params OtherCreateParams

	{ // each field has own block
		if !r.Form.Has("username") {
			w.WriteHeader(http.StatusBadRequest)
			_ = json.NewEncoder(w).Encode(Response{
				Error: "username must me not empty",
			})
			return
		}
		var value string
		stringValue := r.Form.Get("username")
		if stringValue != "" {
			value = stringValue
		}

		if len(value) < 3 {
			w.WriteHeader(http.StatusBadRequest)
			_ = json.NewEncoder(w).Encode(Response{
				Error: "username len must be >= 3",
			})
			return
		}
		params.Username = value
	} // end of field block

	{ // each field has own block
		var value string
		stringValue := r.Form.Get("account_name")
		if stringValue != "" {
			value = stringValue
		}

		params.Name = value
	} // end of field block

	{ // each field has own block
		var value string
		stringValue := r.Form.Get("class")
		if stringValue != "" {
			value = stringValue
		} else {
			value = "warrior"
		}

		enumValues := []string{"warrior", "sorcerer", "rouge"}
		contains := false
		for _, enumValue := range enumValues {
			if enumValue == value {
				contains = true
				break
			}
		}
		if !contains {
			w.WriteHeader(http.StatusBadRequest)
			_ = json.NewEncoder(w).Encode(Response{
				Error: "class must be one of [warrior, sorcerer, rouge]",
			})
			return
		}
		params.Class = value
	} // end of field block

	{ // each field has own block
		var value int
		stringValue := r.Form.Get("level")
		if stringValue != "" {
			value, err = strconv.Atoi(stringValue)
			if err != nil {
				w.WriteHeader(http.StatusBadRequest)
				_ = json.NewEncoder(w).Encode(Response{
					Error: "level must be int",
				})
				return
			}
		}

		if value < 1 {
			w.WriteHeader(http.StatusBadRequest)
			_ = json.NewEncoder(w).Encode(Response{
				Error: "level must be >= 1",
			})
			return
		}

		if value > 50 {
			w.WriteHeader(http.StatusBadRequest)
			_ = json.NewEncoder(w).Encode(Response{
				Error: "level must be <= 50",
			})
			return
		}
		params.Level = value
	} // end of field block

	// вызов оригинального метода
	res, err := h.Create(r.Context(), params)
	// прочие обработки
	response := OtherApiCreateResponse{
		Response: res,
	}
	if err != nil {
		response.Error = err.Error()
		if err, ok := err.(ApiError); ok {
			w.WriteHeader(err.HTTPStatus)
		} else {
			w.WriteHeader(http.StatusInternalServerError)
		}
	}

	_ = json.NewEncoder(w).Encode(response)
}
